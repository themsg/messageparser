messageparser
=============

**messageparser** is a parser that takes a string message and returns a json
object containing the mentions, emoticons, and urls found in the message.

usage
-----
```python
>>> from messageparser import messageparser

changelog
---------

tests
-----

Run unit tests:

    $ nosetests
    .
    ----------------------------------------------------------------------
    Ran 1 test in 0.010s
    OK

