import json
import re
import urllib2

MENTION_REGEX = '(?<=@)[A-Za-z0-9]{1,50}'
# EMOTICON_REGEX = '(?<=\()[A-Za-z]{1,15}(?=\))'
EMOTICON_REGEX = '(?<=\B@)[A-Za-z0-9]{1,50}'

class Parser(object):

    def __init__(self):
	pass

    def parse(self, text):
	res = {}

	mentions = re.findall(MENTION_REGEX, text)
	emoticons = re.findall(EMOTICON_REGEX, text)

	if mentions:
	    res["mentions"] = mentions
        if emoticons:
	    res["emoticons"] = emoticons
        return json.dumps(res)
