import json
import unittest

from messageparser import messageparser

class MessageParserTests(unittest.TestCase):
    def setUp(self):
	self.test_parser = messageparser.Parser()

    def test_mentions1(self):
	test_str = "@chris you around?"
	self.assertEqual(self.test_parser.parse(test_str), 
			json.dumps({"mentions":["chris"]}))

    def test_mentions2(self):
	test_str = "@chris @mike let's meet up"
	self.assertEqual(self.test_parser.parse(test_str), 
			json.dumps({"mentions":["chris", "mike"]}))
    def test_mentions3(self):
	test_str = "hey @mike can you talk to @123 @bob @chris123"
	self.assertEqual(self.test_parser.parse(test_str), 
			json.dumps({"mentions":["mike", "123", "bob", "chris123"]}))

    def test_mentions4(self):
	test_str = "michaelperng@gmail.com"
	self.assertEqual(self.test_parser.parse(test_str), 
			json.dumps({}))

    def test_emoticons1(self):
	test_str = "Good morning! (megusta) (coffee)"
	self.assertEqual(self.test_parser.parse(test_str),
			json.dumps({"emoticons":["megusta", "coffee"]}))

    def test_links1(self):
	pass

    def test_mention_emoticon_mix1(self):
	pass

    def test_mention_link_mix1(self):
	pass

    def test_emoticon_link_mix1(self):
	pass
